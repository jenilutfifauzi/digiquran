<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        // Cek apakah data sudah ada di cache, jika iya, maka ambil data dari cache
        $data = Cache::remember('surat_data', 60 * 24 * 2, function () {
            // Mengambil data dari API jika tidak ada di cache
            $response = Http::get('https://equran.id/api/v2/surat');
            
            // Memparsing data JSON dari response
            return $response->json()['data'];
        });

        return Inertia::render('Home/Index', [
            'logoUrl' => asset('assets/banner.jpg'),
            'listSurah' => $data,
        ]);
    }

    public function show(Request $request, $id)
    {
        // Cek apakah data sudah ada di cache, jika iya, maka ambil data dari cache
        $data = Cache::remember('surat_data_' . $id, 60 * 24 * 2, function () use ($id) {
            // Mengambil data dari API jika tidak ada di cache
            $response = Http::get('https://equran.id/api/v2/surat/' . $id);
            
            // Memparsing data JSON dari response
            return $response->json()['data'];
        });


        return Inertia::render('Home/Show', [
            'logoUrl' => asset('assets/banner.jpg'),
            'detailSurah' => $data,
        ]);
    }
}
