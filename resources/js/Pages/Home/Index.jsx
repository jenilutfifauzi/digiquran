import { Head, Link } from '@inertiajs/react';
import AppLayout from '@/Layouts/AppLayout.jsx';
import Header from '@/Components/Header.jsx';
import Container from '@/Components/Container';
import List from '../Surah/List';
import BannerLogoSvg from '@/Components/BannerLogoSvg';

export default function Home({ logoUrl, listSurah }) {
    return (
        <div className="p-2">
            <Container>
                <Head title="Digital Qur'an" />
                <Header title="Digital Qur'an" subtitle="Assalamu'alaikum" />
                <div className="w-full rounded-lg py-2.5">
                    {/* <img src="assets/banner.svg" className="w-full" /> */}
                    <BannerLogoSvg title="Digital Qur'an" subtitle={'Bacalah walau satu ayat'} />
                </div>
                {/* list surah */}
                <ul>
                    <List surah={listSurah} />
                </ul>
                {/* end list surah */}
            </Container>
        </div>
    );
}

Home.layout = (page) => <AppLayout children={page} />;
