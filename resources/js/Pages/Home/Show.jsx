import { Head, Link } from '@inertiajs/react';
import AppLayout from '@/Layouts/AppLayout.jsx';
import Header from '@/Components/Header.jsx';
import Container from '@/Components/Container';
import {
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Typography,
    Button,
    Tooltip,
    IconButton,
} from '@material-tailwind/react';
import BannerLogoSvg from '@/Components/BannerLogoSvg';

export default function Show({ logoUrl, detailSurah }) {
    console.log(detailSurah);
    return (
        <div className="p-2">
            <Container>
                <Head title="Digital Qur'an" />
                <Header title="Digital Qur'an" subtitle="Assalamu'alaikum" />
                <div className="w-full rounded-lg py-2.5">
                    {/* <img src="../../assets/banner.svg" className="w-full" alt="banner" /> */}
                    <BannerLogoSvg title="Digital Qur'an" subtitle={'Bacalah walau satu ayat'} />
                </div>
                {/* list surah */}
                <Card className="w-full bg-gray-900 shadow-lg">
                    <CardHeader floated={false}>
                        <div className="flex items-center justify-between"></div>
                    </CardHeader>
                    <CardBody>
                        {detailSurah.ayat.map((item) => (
                            <ul>
                                <li key={item.nomorAyat}>
                                    <div>
                                        <div className="mb-3 flex items-center justify-end">
                                            <Typography
                                                variant="h5"
                                                className="font-medium arabic text-rtl text-white text-right"
                                            >
                                                {item.teksArab}
                                            </Typography>
                                        </div>
                                        <Typography color="gray">{item.teksLatin}</Typography>
                                        <Typography color="gray">{item.teksIndonesia}</Typography>
                                        <hr className="mb-4 mt-4" />
                                    </div>
                                </li>
                            </ul>
                        ))}
                    </CardBody>
                    <CardFooter className="pt-3"></CardFooter>
                </Card>

                {/* end list surah */}
            </Container>
        </div>
    );
}

Show.layout = (page) => <AppLayout children={page} />;
