import { useState } from 'react';
import { Tab } from '@headlessui/react';
import { Link } from '@inertiajs/react';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
}

export default function List({ surah }) {
    console.log(surah);
    let [categories] = useState({
        Surah: surah,
    });
    let ListSurah = surah;

    return (
        <div className="mt-2">
            <Tab.Group>
                <Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
                    {Object.keys(categories).map((category) => (
                        <Tab
                            key={category}
                            className={({ selected }) =>
                                classNames(
                                    'w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-white-700',
                                    'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-200 focus:outline-none focus:ring-2',
                                    selected
                                        ? 'bg-violet-300 shadow'
                                        : 'text-blue-100 hover:bg-white/[0.12] hover:text-white',
                                )
                            }
                        >
                            {category}
                        </Tab>
                    ))}
                </Tab.List>

                <Tab.Panels className="mt-2">
                    {Object.values(categories).map((surah) => (
                        <Tab.Panel
                            key={surah.nomor}
                            className={classNames(
                                'rounded-xl bg-blue-900/20 p-3',
                                'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2',
                            )}
                        >
                            <ul>
                                {surah.map((item) => (
                                    <li key={item.nomor} className="relative rounded-md p-3 hover:bg-gray-900">
                                        <div className="flex justify-between items-center">
                                            <h3 className="text-xl font-medium leading-5">{item.namaLatin}</h3>
                                            <h3 className="arabic">{item.nama}</h3>
                                        </div>

                                        <p className="mt-1 flex space-x-1 text-sm font-normal leading-4 text-gray-500 mb-4">
                                            {item.tempatTurun + ' - ' + item.jumlahAyat + ' ayat'}
                                        </p>
                                        <hr />
                                        <Link
                                            href={route('surah.show', { nomor: item.nomor })}
                                            className={classNames(
                                                'absolute inset-0 rounded-md',
                                                'ring-blue-200 focus:z-10 focus:outline-none focus:ring-2',
                                            )}
                                        />
                                    </li>
                                ))}
                            </ul>
                        </Tab.Panel>
                    ))}
                </Tab.Panels>
            </Tab.Group>
        </div>
    );
}
