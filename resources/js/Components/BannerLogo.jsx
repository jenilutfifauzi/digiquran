export default function BannerLogo(props) {
    return (
        <div className="flex flex-col">
            <div className="">
                <img src="assets/banner.svg" className="w-full" />
            </div>
        </div>
    );
}
