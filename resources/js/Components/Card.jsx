export default function Card({ children }) {
    return (
        <div className="w-full bg-gray-900 rounded-lg shadow-lg overflow-hidden flex flex-col md:flex-row max-h-48">
            <div className="w-full">{children}</div>
        </div>
    );
}
