import Navbar from '@/Layouts/Navbar.jsx';
import Footer from '@/Layouts/Footer.jsx';
import Container from '@/Components/Container';

export default function AppLayout({ children }) {
    return (
        <div className="min-h-screen bg-gray-950 text-white">
            <Container>
                <Navbar />
                <main>{children}</main>
                <Footer />
            </Container>
        </div>
    );
}
